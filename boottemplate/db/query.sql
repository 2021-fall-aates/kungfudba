DROP TABLE IF EXISTS Dojo;

DROP TABLE IF EXISTS DojoLookup;

CREATE TABLE Dojo(
    tx_id INTEGER AUTO_INCREMENT,
    PRIMARY KEY (tx_id),
    class_id INTEGER,
    first_name VARCHAR(256),
    last_name VARCHAR(256),
    enroll_date timestamp
);

INSERT INTO Dojo (tx_id, class_id, first_name, last_name, enroll_date)
    VALUES (1, 100, "Bob", "Smack", "2021-02-01 00:00:00"),
        (2, 100, "Smith", "Bobby", "2021-03-02 00:00:01");

CREATE TABLE DojoLookup (
    class_id INTEGER AUTO_INCREMENT,
    PRIMARY KEY (class_id),
    class_name VARCHAR(256),
    instructor VARCHAR(256),
    days VARCHAR(256),
    duration INTEGER
    );

INSERT INTO DojoLookup (class_id, class_name, instructor, days, duration)
    VALUES (100, "Beginner", "Master Puncheir", "T/TH", "90"),
        (112, "Intermediate", "Master Puncheir", "M/W/F", "90");