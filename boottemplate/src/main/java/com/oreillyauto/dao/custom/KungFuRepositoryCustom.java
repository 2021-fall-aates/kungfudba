package com.oreillyauto.dao.custom;

import java.util.List;
import java.util.Map;

import com.oreillyauto.domain.Report;
import com.oreillyauto.domain.Student;

public interface KungFuRepositoryCustom {
    List<Student> getStudents();
    
    Map<String, Integer> getReport();
    
    List<Report> getReportData();
}