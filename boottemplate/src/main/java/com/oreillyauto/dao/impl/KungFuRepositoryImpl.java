package com.oreillyauto.dao.impl;

import java.util.List;
import java.util.Map;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import com.oreillyauto.dao.custom.KungFuRepositoryCustom;
import com.oreillyauto.domain.QReport;
import com.oreillyauto.domain.QStudent;
import com.oreillyauto.domain.Report;
import com.oreillyauto.domain.Student;
import com.querydsl.core.group.GroupBy;

public class KungFuRepositoryImpl extends QuerydslRepositorySupport implements KungFuRepositoryCustom{

    QStudent studentTable = QStudent.student;
    QReport reportTable = QReport.report;
    
    public KungFuRepositoryImpl() {
        super(Student.class);
    }

    @Override
    public List<Student> getStudents() {
        // TODO Auto-generated method stub
        return from(studentTable)
                .fetch();
    }

    @Override
    public Map<String, Integer> getReport() {
        
      return from(reportTable)
              .join(studentTable)
              .on(studentTable.classId.eq(reportTable.classId))
              .transform(GroupBy.groupBy(reportTable.className)
                      .as(reportTable.classId.count().intValue())   
                      );
    }

    @Override
    public List<Report> getReportData() {
       
        return from(reportTable)
                .fetch();
    }
    
}