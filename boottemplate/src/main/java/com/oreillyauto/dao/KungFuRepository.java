package com.oreillyauto.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.oreillyauto.dao.custom.KungFuRepositoryCustom;
import com.oreillyauto.domain.Student;

@Repository
public interface KungFuRepository extends CrudRepository <Student, Integer>, KungFuRepositoryCustom{

    
};