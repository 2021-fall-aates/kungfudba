package com.oreillyauto.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="DojoLookup")
public class Report implements Serializable{

    
    private static final long serialVersionUID = -78675790207418955L;

    public Report() {
        super();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "class_id", columnDefinition = "INTEGER")
    private Integer classId;
    
    @Column(name="class_name", columnDefinition = "VARCHAR(256)")
    private String className;
    
    @Column(name="instructor", columnDefinition = "VARCHAR(256)")
    private String instructor;
    
    @Column(name="days", columnDefinition = "VARCHAR(256)")
    private String days;
    
    @Column(name = "duration", columnDefinition = "INTEGER")
    private Integer duration;

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getInstructor() {
        return instructor;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

   
    
}
