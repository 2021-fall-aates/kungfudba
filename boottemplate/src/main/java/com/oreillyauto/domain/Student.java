package com.oreillyauto.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table (name="dojo")
public class Student implements Serializable{

    private static final long serialVersionUID = -320855916787280699L;

    public Student() {
        super();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "tx_id", columnDefinition = "INTEGER")
    private Integer txId;
    
    @JoinColumn(name="class_id")
    @Column(name = "class_id", columnDefinition = "INTEGER")
    private Integer classId;
    
    @Column(name="first_name", columnDefinition = "VARCHAR(256)")
    private String firstName;
    
    @Column(name="last_name", columnDefinition = "VARCHAR(256)")
    private String lastName;
    
    @Column(name="enroll_date", columnDefinition = "TIMESTAMP")
    private String enrollDate;

    public Integer getTxId() {
        return txId;
    }

    public void setTxId(Integer txId) {
        this.txId = txId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEnrollDate() {
        return enrollDate;
    }

    public void setEnrollDate(String enrollDate) {
        this.enrollDate = enrollDate;
    }
    
}
