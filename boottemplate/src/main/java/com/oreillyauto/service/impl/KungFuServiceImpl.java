package com.oreillyauto.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.KungFuRepository;
import com.oreillyauto.domain.Report;
import com.oreillyauto.domain.Student;
import com.oreillyauto.service.KungFuService;

@Service
public class KungFuServiceImpl implements KungFuService{
    
    @Autowired
    KungFuRepository kungFuRepo;
    
    @Override
    public List<Student> getStudents() {
        
        return kungFuRepo.getStudents();
    }

    @Override
    public Map<String, Integer> getReport() {
        
        return kungFuRepo.getReport();
    }

    @Override
    public List<Report> getReportData() {
        
        return kungFuRepo.getReportData();
    }

    @Override
    public void saveStudent(Student student) {
        
        kungFuRepo.save(student);
        
    }

    @Override
    public void deleteStudentById(Integer id) {
        
        kungFuRepo.deleteById(id);
        
    }

}
