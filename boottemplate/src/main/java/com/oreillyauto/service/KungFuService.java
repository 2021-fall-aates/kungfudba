package com.oreillyauto.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.oreillyauto.domain.Report;
import com.oreillyauto.domain.Student;

@Service
public interface KungFuService {

    List<Student> getStudents();
    Map<String, Integer> getReport();
    List<Report> getReportData();
    
    void saveStudent(Student student);
    void deleteStudentById(Integer id);

}
