package com.oreillyauto.controllers;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.oreillyauto.domain.Report;
import com.oreillyauto.domain.Student;
import com.oreillyauto.service.KungFuService;

@Controller
public class KungFuController{
    
    @Autowired
    KungFuService kungFuService;
    
    @GetMapping (value= {"/kungFu/", "/kungfu/", "/"})
    public String getKungFu(Model model) {
    	return "kungFu";
    }
    
    @GetMapping (value="/reportPage/")
    public String getReportPage(Model model) {
    	model.addAttribute("active", "report");
        return "reportPage";
    }
    
    @ResponseBody
    @GetMapping (value="/kungFu/getStudents")
    public List<Student> getStudentList(){
    	try {
    		return kungFuService.getStudents();
    	} catch(Exception e) {
    		e.printStackTrace();
    		return Collections.emptyList();
    	}
    }
    
    @ResponseBody
    @GetMapping(value="/reportPage/getReport")
    public List<Report> getReportData(){
        return kungFuService.getReportData();
    }
    
    @ResponseBody
    @GetMapping(value="/reportPage/report")
    public Map<String, Integer> getReport(){        
        return kungFuService.getReport();
    }
    
    @ResponseBody
    @PostMapping (value="/kungFu/saveStudent")
    public void saveStudent(@RequestBody Student student) {
        try {
            System.out.println(student);
            kungFuService.saveStudent(student);
        } catch(Exception e) {
            System.out.println(e);
        }
    }
    
   
    @GetMapping (value="/kungFu/deleteStudent")
    public String deleteStudent(@RequestParam String id) {
        System.out.println(id);
        try {
            System.out.println(Integer.valueOf(id));
            kungFuService.deleteStudentById(Integer.valueOf(id));
        } catch(Exception e) {
            System.out.println(e);
        }
        
        return "kungFu";
    }
    
    
}