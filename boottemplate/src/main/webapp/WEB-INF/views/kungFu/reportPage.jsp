<%@ include file="/WEB-INF/layouts/include.jsp" %>

<div class="container border">
	<div class="row">
		<div class="col-12">
			<div class="center">
				<h1><center>Kung Fu DBA</center></h1>
			</div>
		</div>
	</div>
	<br />
	<div class="row">
		<div class="col-12">
			<div id="transactionReportTitleDiv" class="center">
				<h3>*Transaction Report Title*</h3>
			</div>
		</div>
	</div>
	<br />
	<div class="row">
		<div class="col-sm-6">
			<h5><b>Total Transactions: <span id=reportTotalTransactions>#</span></b></h5>
		</div>
		<br />
		<div class="col-sm-6">
			<h5><b>Report Date: <span id=reportDateTime>#</span></b></h5>
		</div>
	</div>
	
			<br />
	<div class="row">
		<div class="col-12">
			<h5><b>Group By: </b></h5>
			<br />
			<h5><b>&nbsp;&nbsp;&nbsp;Description: </b></h5>
		</div>
	</div>
	<div class="row p-1">
		<div class="col-12">
			<div id="table1">
				<orly-table id="table" loaddataoncreate
				    url="<c:url value='/reportPage/getReport' />"
				    maxrows="10"
				    class="invisible"
				    bordered>
				    
				    <orly-column field="classId" label="Class ID" class=""></orly-column>
				    <orly-column field="className" label="Class Name" class=""></orly-column>
				    <orly-column field="instructor" label="Instructor" class=""></orly-column>
				    <orly-column field="days" label="Days" class=""></orly-column>
				    <orly-column field="duration" label="Duration" class=""></orly-column>
				</orly-table>
			</div>
		</div>
	</div>
	<hr>
	
	<div class="row">
		<div class="col-12">
			<h5><b>Group By: </b></h5>
			<br />
			<h5><b>&nbsp;&nbsp;&nbsp;Description: </b></h5>
		</div>
	</div>
	<div class="row p-1">
		<div class="col-12">
			<div id="table1">
				<orly-table id="table" loaddataoncreate
				    url="<c:url value='/reportPage/getReport' />"
				    maxrows="10"
				    class="invisible"
				    bordered>
				    
				    <orly-column field="classId" label="Class ID" class=""></orly-column>
				    <orly-column field="className" label="Class Name" class=""></orly-column>
				    <orly-column field="instructor" label="Instructor" class=""></orly-column>
				    <orly-column field="days" label="Days" class=""></orly-column>
				    <orly-column field="duration" label="Duration" class=""></orly-column>
				</orly-table>
			</div>
		</div>
	</div>
	<hr>
	
	<div class="row">
		<div class="col-12">
			<h5><b>Group By: </b></h5>
			<br />
			<h5><b>&nbsp;&nbsp;&nbsp;Description: </b></h5>
		</div>
	</div>
	<div class="row p-1">
		<div class="col-12">
			<div id="table1">
				<orly-table id="table" loaddataoncreate
				    url="<c:url value='/reportPage/getReport' />"
				    maxrows="10"
				    class="invisible"
				    bordered>
				    
				    <orly-column field="classId" label="Class ID" class=""></orly-column>
				    <orly-column field="className" label="Class Name" class=""></orly-column>
				    <orly-column field="instructor" label="Instructor" class=""></orly-column>
				    <orly-column field="days" label="Days" class=""></orly-column>
				    <orly-column field="duration" label="Duration" class=""></orly-column>
				</orly-table>
			</div>
		</div>
	</div>
	
	
	
</div>

<script>
orly.ready.then(()=> {
	
	orly.on(orly.qid("newStudentBtn"), "click", (e)=>{
		window.location.href = "http://www.localhost:8080/kungFu/";
	});
})

</script>
