<%@ include file="/WEB-INF/layouts/include.jsp" %>

<h1>Kung Fu DBA</h1>

<div id="addStudentDiv" class="d-none pt-2">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<c:if test="${not empty student.id}">
					<h2 id="addEditTitle" class="pl-3">Edit Student</h2>
				</c:if>
				<c:if test="${empty student.id}"> 
					<h2 id="addEditTitle" class="pl-3">Add Student</h2>
				</c:if>
				<script>console.log(${model.txId})</script>
				
				<div class="col-5">
					<hr>
				</div>
				    
			    <form id="addStudentForm" is="orly-form">
			        
			        <input class="form-control" id="txId" name="txId" type="hidden">
			        <input class="form-control" id="classId" name="classId" type="hidden" value=0>
			
			        <div class="form-group col-12 col-sm-8 col-md-6 col-lg-4">
			            <label for="enrollDate">Enrollment Date</label>
			            <input required class="form-control" id="enrollDate" name="enrollDate" type="date">
			        </div>
			        
			        <div class="form-group col-12 col-sm-8 col-md-6 col-lg-4">
			            <label for="firstName">First Name</label>
			            <input required class="form-control" id="firstName" name="firstName" type="text">
			        </div>
			        
			        <div class="form-group col-12 col-sm-8 col-md-6 col-lg-4 ">
			            <label for="lastName">Last Name</label>
			            <input required class="form-control" id="lastName" name="lastName" type="text">
			        </div>
			        
			        <div class="pl-3">
						<button id="btnSubmit" type="button" class="btn btn-primary pl-3">Submit</button>
				        <button id="btnReset" type="reset" class="btn btn-danger">Reset</button>
				        <button id="btnCancel" type="button" class="btn btn-warning">Cancel</button>
					</div>
			        
			    </form>
		    </div>
		</div>
	</div>
	
    
</div>


<!-- for DojoLookup -->
	<div id="studentTable">
        <orly-table id="table" loaddataoncreate
            url="<c:url value='/kungFu/getStudents' />" 
            includefilter 
            maxrows="10"
            class="invisible"
            bordered>
            <orly-column field="action" label="Action" class="">
                <div slot="cell">
                
                    <orly-icon color="cornflowerblue" name="edit" id="\${`e_\${model.txId}`}"></orly-icon>
                    
                    <a href="\${`<c:url value="/kungFu/deleteStudent?id=\${model.txId}" />`}">
                    <orly-icon color="red" name="x" id="\${`d_\${model.txId}`}"></orly-icon>
                    </a>
                </div>
            </orly-column>
            <orly-column field="txId" label="tx ID" class="" type="hidden"></orly-column>
            <orly-column field="classId" label="Class ID" class=""></orly-column>
            <orly-column field="firstName" label="First Name" class=""></orly-column>
            <orly-column field="lastName" label="Last Name" class=""></orly-column>
            <orly-column class="col-md-3" field="enrollDate" label="Enroll Date"></orly-column>
        </orly-table>
    </div>
    

<script>
	
	orly.ready.then(function(){
	
		orly.on(orly.qid("newStudentBtn"), "click", (e)=>{
			show("addStudentDiv");
			hide("studentTable");
		})
		
		orly.on(orly.qid("btnCancel"), "click", ()=> {
			show("studentTable");
			hide("addStudentDiv");
		});
		
		orly.on(orly.qid("btnReset"), "click", ()=> {
			orly.qid("addStudentForm").reset();
		});
		
		orly.on(orly.qid("btnSubmit"), "click", () => {
			submitForm();
		});
		
		orly.on(orly.qid("studentTable"), "click", (e) => {
			
			if (e.target.tagName == "ORLY-ICON"){
				let targetId = e.target.id;
				if (targetId.startsWith("e_")){
					/* alert("ready to edit id = " + targetId.slice(2) + "!"); */
					editRow(targetId.slice(2));
				}
				if (targetId.startsWith("d_")){
					//alert("ready to delete id = " + targetId.slice(2) + "!");
					/* deleteRow(targetId.slice(2)); */
				}
			}
		});
	})
		
	function show(id){
		orly.qid(id).classList.remove("d-none");
	}
	
	function hide(id){
		orly.qid(id).classList.add("d-none");
	}
	
	function submitForm() {
		let url = '<c:url value="/kungFu/saveStudent"/>';
		let data = orly.qid('addStudentForm').values;
		let options = {
				method: "POST",
				body: JSON.stringify(data),
				headers: {"content-type" : "application/json"}
		};
		
		fetch(url, options).then(function(response){
			console.log(response);
			if(response.ok){
				//handling promise from fetch
				let message = response.json();
				return message;
			} else{
				alert('error' + response);
			}
		}).then(function(notMessage){
			//grabbing fresh table data
			orly.qid("studentTable").loadData();
			let jsonObject = JSON.parse(notMessage);
			console.log(jsonObject);
		}).catch(function(e){
			console.log(e);
		});
		
		console.log("submitting form*");
		
	}
	
	function editRow(id){
		
		/* ${student.id} = id; */
		let txId = orly.qid("txId");
		txId.value = id;
		show("addStudentDiv");
		hide("studentTable");
	}
	
	/* function deleteRow(id){
		let url = '<c:url value="/kungFu/deleteStudent"/>';
		
		fetch(url).then(function(response){
			if(response.ok){
				let message = response.json();
				return message;
			}
			else{
				console.log("error " + response)
			}
		}).then(function(deletedItem){
			let jsonObject = JSON.parse(deletedItem);
			console.log(jsonObject);
		}).catch(function(e){
			console.log(e);
		});
	} */

</script>
