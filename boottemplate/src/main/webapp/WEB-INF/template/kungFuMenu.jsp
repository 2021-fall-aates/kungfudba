<%@ include file="/WEB-INF/layouts/include.jsp"%>

<div class="card">
	  <div class="card-header">
	    <a href="/kungFu/">Menu</a>
	  </div>

	<nav class="nav nav-pills flex-column mt-2 mb-2">
		<ul class="list-group list-group-flush list-unstyled pl-2 pr-2">
			<li> 
			   <a class="${active eq 'new' ? 'nav-item nav-link active' : 'nav-item nav-link'}" 
			   id="newStudentBtn" href="javascript:void(0);">New Student</a>
			</li>
	 		<li>
				<a class="${active eq 'report' ? 'nav-item nav-link active' : 'nav-item nav-link'}" 
				href="<c:url value='/reportPage/'/>">Course Report</a>
			</li>
		</ul>
	
	</nav>
</div>